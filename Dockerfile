ARG RUST_VERSION

FROM rust:${RUST_VERSION}

RUN rustup component add rust-analysis --toolchain ${RUST_VERSION}-x86_64-unknown-linux-gnu
RUN rustup component add rust-src --toolchain ${RUST_VERSION}-x86_64-unknown-linux-gnu
RUN rustup component add rls --toolchain ${RUST_VERSION}-x86_64-unknown-linux-gnu