# rust

Provides a docker image that can be used for Rust development using Visual
Studio Code Development Containers.

## Usage

```json
{
    "image": "registry.gitlab.com/ljpcore/devcontainer/rust:1.50.0"
}
```
