#!/bin/bash
set -e

RUST_VERSION="1.58.1"

echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
docker build --build-arg RUST_VERSION=$RUST_VERSION -t $CI_REGISTRY/ljpcore/devcontainer/rust:$RUST_VERSION .
docker push $CI_REGISTRY/ljpcore/devcontainer/rust:$RUST_VERSION
